const mongoose = require('mongoose');

mongoose.connection.on('error', (err) => {
    logger.error(`MongoDB connection error: ${err}`);
    process.exit(-1);
});


exports.connect = () => {
    mongoose.connect('mongodb://127.0.0.1:27017/jiraTemplate', {
        keepAlive: 1,
        useNewUrlParser: true,
        useCreateIndex: true,
    });
    return mongoose.connection;
};

