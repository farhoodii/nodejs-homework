const path = require('path')
const Resize = require('../../helper/resize');

async function uploadFile(req, res) {
    const imagePath = path.join(__dirname, '../../../public/images');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.status(401).json({ error: 'Please provide an image' });
    }
    const filename = await fileUpload.save(req.file.buffer);
    return res.status(200).send(`${filename} is now in images folder.`);
}
module.exports = { uploadFile }