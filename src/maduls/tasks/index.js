
var { Task } = require('../../models/task')

async function allTasks(req, res) {
    const { limit, offset } = req.query
    try {
        const response = await Task.find({}).skip(parseInt(offset, 10)).limit(parseInt(limit, 10))
        if (response.length === 0) {
            res.send("task doesn't exist")
        } else {
            res.json(response)
        }
    }
    catch (error) {
        res.send(error)
    }

}

async function oneTask(req, res) {
    let querydata = req.params
    let { _id } = querydata
    try {
        const response = await Task.findOne({ _id })
        res.json(response)
    }
    catch (error) {
        res.send('task doesn`t exist')
    }
}

async function editTask(req, res) {
    let data = req.body
    let querydata = req.params
    var { _id } = querydata
    try {
        await Task.updateOne({ _id }, data)
        res.send('updated')
    }
    catch (error) {
        res.send('task doesn`t exist')
    }

}

async function addTask(req, res) {
    let data = req.body
    var task = new Task(data)
    try {
        const response = await task.save()
        res.send(response.name + " " + "saved to tasks collection.");
    }
    catch (error) {
        res.send(error)
    }
}

async function deleteTask(req, res) {
    let querydata = req.params
    var { _id } = querydata
    try {
        await Task.deleteOne({ _id })
        res.send('task deleted')
    }
    catch (error) {
        res.send('task doesn`t exist')
    }
}

module.exports = { allTasks, oneTask, editTask, addTask, deleteTask }