const Joi = require('joi')

const addScheema = Joi.object({
    name: Joi.string().required(),
    username:Joi.string().required(),
    description:Joi.string().required(),
    en_name:Joi.string().required(),
    deadline:Joi.string().required()

  })
  

const validations={addScheema}

module.exports = validations