const mongoose = require('mongoose');
const s = require('./names');

const schema = new mongoose.Schema({
        name: String,
        en_name: String,
        username:String,
        description:String,
        deadline:String
    });

module.exports = mongoose.model(s.model_name, schema);

