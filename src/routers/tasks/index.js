var express = require('express');
var router = express.Router();
const multer = require('multer');
const upload = multer();
const validator = require('express-joi-validation').createValidator({})
const { addScheema } = require('../../models/validation')
const { allTasks, oneTask, editTask, addTask, deleteTask } = require('../../maduls/tasks')

router.get('/', allTasks)
router.post('/', upload.none(), addTask)
router.get('/:_id', oneTask)
router.put('/:_id', upload.none(), validator.body(addScheema), editTask)
router.delete('/:_id', upload.none(), deleteTask)

module.exports = router;
