var express = require('express')
var tasks = require("./src/routers/tasks/index")
var upload = require("./src/routers/upload/index")
var user = require("./src/routers/user/index")

var session = require("express-session");
var bodyParser = require("body-parser");
const passport = require('passport')

require('./src/auth/passport');


var app = express()

app.use(express.static("public"));
app.use(session({ secret: "cats" }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use(express.static('public'));
app.use('/tasks', tasks)
app.use('/upload', upload)
app.use('/user', user)



app.listen(3200, console.log('say hello to port on 3200'))

